package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_04"
)

func main() {
	records := day_04.Parse("../input.txt")
	var mostSleepyRecord *day_04.SleepRecord = nil
	mostSleepyGuardId := 0
	for id, sleepRecord := range records {
		if mostSleepyRecord != nil {
			_, testAmount := sleepRecord.MostSleptMinute()
			_, maxAmount := mostSleepyRecord.MostSleptMinute()
			if testAmount > maxAmount {
				mostSleepyRecord = sleepRecord
				mostSleepyGuardId = id
			}
		} else if mostSleepyRecord == nil {
			mostSleepyRecord = sleepRecord
			mostSleepyGuardId = id
		}
	}

	minute, _ := mostSleepyRecord.MostSleptMinute()
	fmt.Println(mostSleepyGuardId * minute)
}
