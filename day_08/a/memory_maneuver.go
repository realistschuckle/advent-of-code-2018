package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_08"
)

func main() {
	n := day_08.Parse("../input.txt")

	fmt.Println(n.SumMeta())
}
