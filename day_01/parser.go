package day_01

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func Parse(path string) (values []int) {
	f, err := os.Open(path)
	if err != nil {
		os.Exit(1)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if val, err := strconv.Atoi(line); err == nil {
			values = append(values, val)
		} else {
			fmt.Printf("Error reading %s\n", line)
			os.Exit(2)
		}
	}
	return
}
