package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_01"
)

func main() {
	values := day_01.Parse("../input.txt")

	sum := 0
	sums := make(map[int]int)
	found := false
	for found == false {
		for _, val := range values {
			sum += val
			sums[sum] += 1
			if sums[sum] > 1 {
				found = true
				break
			}
		}
	}

	fmt.Println(sum)
}
