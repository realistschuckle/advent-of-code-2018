package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_01"
)

func main() {
	sum := 0
	for _, entry := range day_01.Parse("../input.txt") {
		sum += entry
	}

	fmt.Println(sum)
}
