package main

import "fmt"

func main() {
	serialNumber := 1133
	powerLevels := make([][]int, 300)
	for row := 0; row < 300; row += 1 {
		powerLevels[row] = make([]int, 300)
		for col := 0; col < 300; col += 1 {
			rackId := (col + 1) + 10
			powerLevel := rackId * (row + 1)
			powerLevel += serialNumber
			powerLevel *= rackId
			powerLevel = (powerLevel / 100) % 10
			powerLevel -= 5
			powerLevels[row][col] = powerLevel
		}
	}

	windows := make([][][]int, 300)
	for row := 0; row < 300; row += 1 {
		windows[row] = make([][]int, 300)
		for col := 0; col < 300; col += 1 {
			windows[row][col] = make([]int, 300)
		}
	}

	for row := 0; row < 300; row += 1 {
		for col := 0; col < 300; col += 1 {
			go func(row, col int) {
				squareDim := col
				if row > col {
					squareDim = row
				}
				for dim := 1; dim < 300-squareDim; dim += 1 {
					sum := 0
					for dy := 0; dy < dim; dy += 1 {
						for dx := 0; dx < dim; dx += 1 {
							sum += powerLevels[row+dy][col+dx]
						}
					}
					windows[row][col][dim] = sum
				}
			}(row, col)
		}
	}

	defer func() {
		var x, y, d, max int
		max = 0
		for row := 0; row < 298; row += 1 {
			for col := 0; col < 298; col += 1 {
				for dim := 0; dim < 300; dim += 1 {
					sum := windows[row][col][dim]
					if sum > max {
						max = sum
						x = col + 1
						y = row + 1
						d = dim
					}
				}
			}
		}
		fmt.Printf("%d,%d,%d\n", x, y, d)
	}()
}
