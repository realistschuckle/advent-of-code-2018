package main

import "fmt"

func main() {
	serialNumber := 1133
	powerLevels := make([][]int, 300)
	for row := 0; row < 300; row += 1 {
		powerLevels[row] = make([]int, 300)
		for col := 0; col < 300; col += 1 {
			rackId := (col + 1) + 10
			powerLevel := rackId * (row + 1)
			powerLevel += serialNumber
			powerLevel *= rackId
			powerLevel = (powerLevel / 100) % 10
			powerLevel -= 5
			powerLevels[row][col] = powerLevel
		}
	}

	var x, y, max int
	windows := make([][]int, 298)
	max = 0
	for row := 0; row < 298; row += 1 {
		windows[row] = make([]int, 298)
		for col := 0; col < 298; col += 1 {
			sum := 0
			for dy := 0; dy < 3; dy += 1 {
				for dx := 0; dx < 3; dx += 1 {
					sum += powerLevels[row+dy][col+dx]
				}
			}
			windows[row][col] = sum
			if sum > max {
				max = sum
				x = col + 1
				y = row + 1
			}
		}
	}

	fmt.Printf("%d,%d\n", x, y)
}
