package day_12

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Hall struct {
	left         int
	pots         []bool
	instructions map[int]bool
}

func (h *Hall) Grow() {
	pow2 := []int{1, 2, 4, 8, 16}
	newState := []bool{}
	h.left -= 2
	newState = append(newState, false)
	newState = append(newState, false)
	newState = append(newState, false)
	newState = append(newState, false)
	for i := 0; i < len(h.pots)-4; i += 1 {
		sum := 0
		for x := 0; x < 5; x += 1 {
			if h.pots[i+x] {
				sum += pow2[x]
			}
		}
		newState = append(newState, h.instructions[sum])
	}
	newState = append(newState, false)
	newState = append(newState, false)
	newState = append(newState, false)
	newState = append(newState, false)
	h.pots = newState
}

func (h *Hall) Sum() (sum int) {
	pots := h.pots[-h.left+2:]
	for i := 0; i < len(pots); i += 1 {
		if pots[i] {
			sum += i
		}
	}
	return
}

func (h *Hall) Print() {
	pots := h.pots[-h.left:]
	for i := 2; i < len(pots)-2; i += 1 {
		if pots[i] {
			fmt.Print("#")
		} else {
			fmt.Print(".")
		}
	}
	fmt.Println()
}

func Parse(path string) (hall *Hall) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	pow2 := []int{1, 2, 4, 8, 16}
	i := 0
	scanner := bufio.NewScanner(f)
	hall = &Hall{-2, []bool{}, make(map[int]bool)}
	hall.pots = append(hall.pots, false)
	hall.pots = append(hall.pots, false)
	hall.pots = append(hall.pots, false)
	hall.pots = append(hall.pots, false)
	for scanner.Scan() {
		line := scanner.Text()
		if i == 0 {
			initialState := line[15:]
			for _, r := range initialState {
				if r == '.' {
					hall.pots = append(hall.pots, false)
				} else if r == '#' {
					hall.pots = append(hall.pots, true)
				}
			}
		} else if len(strings.TrimSpace(line)) > 0 {
			sum := 0
			for i := 0; i < 5; i += 1 {
				if line[i] == '#' {
					sum += pow2[i]
				}
			}
			hall.instructions[sum] = line[9] == '#'
		}
		i += 1
	}
	hall.pots = append(hall.pots, false)
	hall.pots = append(hall.pots, false)
	hall.pots = append(hall.pots, false)
	hall.pots = append(hall.pots, false)
	return
}
