package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_12"
)

func main() {
	hall := day_12.Parse("../input.txt")
	for i := 0; i < 50000000000; i += 1 {
		hall.Grow()
	}
	fmt.Println(hall.Sum())
}
