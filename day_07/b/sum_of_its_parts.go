package main

import (
	"bytes"
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_07"
)

func allDone(nodes []day_07.Node) (done bool) {
	done = true
	for _, n := range nodes {
		done = done && n.Done()
	}
	return
}

func contains(list []day_07.Node, node day_07.Node) (result bool) {
	for _, n := range list {
		if n != nil && node.Value() == n.Value() {
			result = true
			break
		}
	}
	return
}

func remove(s []day_07.Node, i int) []day_07.Node {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func minNode(nodes []day_07.Node) (index int, n day_07.Node) {
	if len(nodes) > 0 {
		n = nodes[0]
		for i, node := range nodes {
			if node.Value() < n.Value() {
				index = i
				n = node
			}
		}
	}
	return
}

func schedule(workers []day_07.Node, node day_07.Node) (scheduled bool) {
	scheduled = false
	if node != nil {
		for i, n := range workers {
			if n == nil {
				workers[i] = node
				scheduled = true
				break
			}
		}
	}
	return
}

func main() {
	workers := []day_07.Node{nil, nil, nil, nil, nil}
	nodes := day_07.Parse("../input.txt", false, 60)
	// nodes := day_07.Manually([][]string{[]string{"C", "A"}, []string{"C", "F"}, []string{"A", "B"}, []string{"A", "D"}, []string{"B", "E"}, []string{"D", "E"}, []string{"F", "E"}}, false, 0)
	b := new(bytes.Buffer)
	actives := []day_07.Node{}

	for _, node := range nodes {
		if node.Active() {
			actives = append(actives, node)
		}
	}

	time := 1
	for !allDone(nodes) {
		for {
			i, minNode := minNode(actives)
			if schedule(workers, minNode) {
				minNode.Use()
				actives = remove(actives, i)
			} else {
				break
			}
		}
		for _, node := range nodes {
			node.Tick()
			if node.Active() && !contains(actives, node) {
				actives = append(actives, node)
			}
			if node.Done() {
				for i, worker := range workers {
					if worker == node {
						b.WriteString(node.Value())
						workers[i] = nil
					}
				}
			}
		}
		time += 1
	}

	fmt.Println(time)
}
