package day_07

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

type Node interface {
	Value() string
	Active() bool
	Nexts() []Node
	Use()
	Tick()
	Done() bool
}

type edgeCollection struct {
	edges []*edge
}

func newEdgeCollection() *edgeCollection {
	return &edgeCollection{[]*edge{}}
}

func (e *edgeCollection) add(ed *edge) {
	e.edges = append(e.edges, ed)
}

func (e *edgeCollection) active() (result bool) {
	result = true
	for _, e := range e.edges {
		result = result && e.activated
	}
	return
}

type node struct {
	value    string
	incoming *edgeCollection
	outgoing *edgeCollection
	used     bool
	timeLeft byte
	timeless bool
}

func newNode(value string, timeless bool, delta byte) (n *node) {
	n = &node{value, newEdgeCollection(), newEdgeCollection(), false, value[0] - 'A' + 1 + delta, timeless}
	return
}

func (n *node) active() (result bool) {
	result = !n.used && n.incoming.active()
	return
}

func (n *node) Active() bool {
	return n.active()
}

func (n *node) Value() string {
	return n.value
}

func (n *node) Nexts() (nodes []Node) {
	nodes = []Node{}
	for _, e := range n.outgoing.edges {
		nodes = append(nodes, e.to)
	}
	return
}

func (n *node) Use() {
	n.used = true
	if n.timeless {
		n.timeLeft = 0
		for _, e := range n.outgoing.edges {
			e.activated = true
		}
	}
}

func (n *node) Tick() {
	if n.used && n.timeLeft > 0 {
		n.timeLeft -= 1
	}
	if n.timeLeft == 0 {
		for _, e := range n.outgoing.edges {
			e.activated = true
		}
	}
}

func (n *node) Done() bool {
	return n.used && n.timeLeft == 0
}

type edge struct {
	from      *node
	to        *node
	activated bool
}

func newEdge(from *node, to *node) (e *edge) {
	e = &edge{from, to, false}
	return
}

func Manually(descs [][]string, timeless bool, delta byte) (nodes []Node) {
	inside := make(map[string]*node)
	nodes = []Node{}

	for _, desc := range descs {
		fromName := desc[0]
		toName := desc[1]
		if inside[fromName] == nil {
			inside[fromName] = newNode(fromName, timeless, delta)
			nodes = append(nodes, inside[fromName])
		}
		if inside[toName] == nil {
			inside[toName] = newNode(toName, timeless, delta)
			nodes = append(nodes, inside[toName])
		}
		from := inside[fromName]
		to := inside[toName]
		e := newEdge(from, to)
		from.outgoing.add(e)
		to.incoming.add(e)
	}

	return
}

func Parse(path string, timeless bool, delta byte) (nodes []Node) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	coords := regexp.MustCompile(`^Step (.) must be finished before step (.) can begin\.$`)

	i := 0
	scanner := bufio.NewScanner(f)
	inside := make(map[string]*node)
	nodes = []Node{}

	for scanner.Scan() {
		line := scanner.Text()
		i += 1

		if tokens := coords.FindStringSubmatch(line); tokens != nil {
			fromName := tokens[1]
			toName := tokens[2]
			if inside[fromName] == nil {
				inside[fromName] = newNode(fromName, timeless, delta)
				nodes = append(nodes, inside[fromName])
			}
			if inside[toName] == nil {
				inside[toName] = newNode(toName, timeless, delta)
				nodes = append(nodes, inside[toName])
			}
			from := inside[fromName]
			to := inside[toName]
			e := newEdge(from, to)
			from.outgoing.add(e)
			to.incoming.add(e)
		} else {
			fmt.Println("Could not parse line '" + line + "'")
			os.Exit(2)
		}
	}

	return
}
