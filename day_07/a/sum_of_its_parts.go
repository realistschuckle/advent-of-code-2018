package main

import (
	"bytes"
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_07"
)

func contains(list []day_07.Node, node day_07.Node) (result bool) {
	for _, n := range list {
		if node.Value() == n.Value() {
			result = true
			break
		}
	}
	return
}

func remove(s []day_07.Node, i int) []day_07.Node {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func minNode(nodes []day_07.Node) (index int, n day_07.Node) {
	n = nodes[0]
	for i, node := range nodes {
		if node.Value() < n.Value() {
			index = i
			n = node
		}
	}
	return
}

func main() {
	nodes := day_07.Parse("../input.txt", true, 0)
	b := new(bytes.Buffer)
	actives := []day_07.Node{}

	for _, node := range nodes {
		if node.Active() {
			actives = append(actives, node)
		}
	}

	for len(actives) > 0 {
		i, minNode := minNode(actives)
		minNode.Use()
		b.WriteString(minNode.Value())
		actives = remove(actives, i)
		for _, node := range nodes {
			if node.Active() && !contains(actives, node) {
				actives = append(actives, node)
			}
		}
	}

	fmt.Println(b.String())
}
