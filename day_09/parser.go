package day_09

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type Condition struct {
	NumberOfPlayers   int
	LastMarblePlayed  int
	ExpectedAnswer    int
	HasExpectedAnswer bool
}

type node struct {
	value int
	prev  *node
	next  *node
}

func newNode(value int, prev *node, next *node) *node {
	return &node{value, prev, next}
}

type CircularQueue struct {
	current *node
}

func NewCircularQueue() CircularQueue {
	n := &node{0, nil, nil}
	n.prev = n
	n.next = n

	return CircularQueue{n}
}

func Play(cq *CircularQueue, turn int, playerScores []int) (num int) {
	if turn%23 == 0 {
		for i := 0; i < 7; i += 1 {
			cq.current = cq.current.prev
		}
		num = turn + cq.current.value
		playerScores[(turn-1)%len(playerScores)] += num
		cq.current.prev.next = cq.current.next
		cq.current.next.prev = cq.current.prev
		cq.current = cq.current.next
	} else {
		insert := cq.current.next
		afterInsert := insert.next
		n := newNode(turn, insert, afterInsert)
		insert.next = n
		afterInsert.prev = n
		cq.current = n
	}
	return
}

func Parse(path string) []Condition {
	hasAnswerPattern := regexp.MustCompile(`(\d+) players; last marble is worth\ (\d+) points: high score is (\d+)`)
	noAnswerPattern := regexp.MustCompile(`(\d+) players; last marble is worth (\d+) points`)

	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	conds := []Condition{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if line[0] == '#' {
			continue
		}
		if tokens := hasAnswerPattern.FindStringSubmatch(line); tokens != nil {
			numPlayers, _ := strconv.Atoi(tokens[1])
			lastPlayed, _ := strconv.Atoi(tokens[2])
			expected, _ := strconv.Atoi(tokens[3])
			conds = append(conds, Condition{numPlayers, lastPlayed, expected, true})
		} else if tokens := noAnswerPattern.FindStringSubmatch(line); tokens != nil {
			numPlayers, _ := strconv.Atoi(tokens[1])
			lastPlayed, _ := strconv.Atoi(tokens[2])
			conds = append(conds, Condition{numPlayers, lastPlayed, 0, false})
		} else {
			log.Fatal("Could not parse line", line, " in ", path)
			os.Exit(2)
		}
	}
	return conds
}
