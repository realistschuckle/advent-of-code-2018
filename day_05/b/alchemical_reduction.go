package main

import (
	"fmt"
	"math"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_05"
)

func main() {
	basePolymer := day_05.Parse("../input.txt")
	runes := basePolymer.DistinctRunes()

	lengths := make(chan []int)

	for ri, r := range runes {
		go func(ri int, r rune, basePolymer day_05.Polymer) {
			filtered := basePolymer.Filter(r)
			result := filtered.Reduce()
			lengths <- []int{ri, result.Length()}
		}(ri, r, basePolymer)
	}

	finalLengths := make([]int, len(runes))
	shortestLength := math.MaxInt32
	for _, _ = range runes {
		vals := <-lengths
		i := vals[0]
		l := vals[1]
		finalLengths[i] = l
		if l < shortestLength {
			shortestLength = l
		}
	}

	fmt.Println(shortestLength)
}
