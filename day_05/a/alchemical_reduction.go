package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_05"
)

func main() {
	polymer := day_05.Parse("../input.txt")
	polymer = polymer.Reduce()
	fmt.Println(polymer.Length())
}
