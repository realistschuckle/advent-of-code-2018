package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_06"
)

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func calcTopo(s day_06.Space) (topo [][]int) {
	width, height := s.Dimensions()
	topo = make([][]int, height)
	for i, _ := range topo {
		topo[i] = make([]int, width)
	}

	for row := 0; row < height; row += 1 {
		for col := 0; col < width; col += 1 {
			sum := 0
			for _, a := range s.Areas() {
				sum += abs(row-a.Y()) + abs(col-a.X())
			}
			if sum < 10000 {
				topo[row][col] = 1
			}
		}
	}

	return
}

func main() {
	s := day_06.Parse("../input.txt")
	m := calcTopo(s)
	size := 0
	for row := 0; row < len(m); row += 1 {
		for col := 0; col < len(m[row]); col += 1 {
			size += m[row][col]
		}
	}
	fmt.Println(size)
}
