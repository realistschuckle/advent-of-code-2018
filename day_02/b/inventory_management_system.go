package main

import (
	"bytes"
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_02"
)

func main() {
	lines := day_02.Parse("../input.txt")
	var common_runes []rune

	for x, line := range lines {
		line_runes := bytes.Runes([]byte(line))

		for _, l := range lines[x+1:] {
			string_diff := 0
			for i, r := range l {
				if r != line_runes[i] {
					string_diff += 1
				}
			}
			if string_diff == 1 {
				for i, r := range l {
					if r == line_runes[i] {
						common_runes = append(common_runes, r)
					}
				}
			}
		}
	}

	fmt.Println(string(common_runes))
}
