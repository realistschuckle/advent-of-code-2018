package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_03"
)

func main() {
	rects := day_03.Parse("../input.txt")

	has_overlaps := make([]bool, len(rects))
	for x := 0; x < len(rects)-1; x += 1 {
		for y := x + 1; y < len(rects); y += 1 {
			first_rect := rects[x]
			second_rect := rects[y]
			overlap := first_rect.Overlap(second_rect)
			has_overlap := len(overlap) > 0
			has_overlaps[x] = has_overlaps[x] || has_overlap
			has_overlaps[y] = has_overlaps[y] || has_overlap
		}
	}

	for x := 0; x < len(has_overlaps); x += 1 {
		if !has_overlaps[x] {
			fmt.Println(rects[x].Id)
		}
	}
}
