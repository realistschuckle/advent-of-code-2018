package day_03

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

type Rect struct {
	Id     int
	left   int
	top    int
	width  int
	height int
}

type Coordinate struct {
	X int
	Y int
}

func (r Rect) Bottom() int {
	return r.top + r.height
}

func (r Rect) Right() int {
	return r.left + r.width
}

func min(x int, y int) int {
	if x < y {
		return x
	}
	return y
}

func max(x int, y int) int {
	if x < y {
		return y
	}
	return x
}

func (r Rect) Overlap(other Rect) (coords []Coordinate) {
	left := max(r.left, other.left)
	top := max(r.top, other.top)
	right := min(r.Right(), other.Right())
	bottom := min(r.Bottom(), other.Bottom())

	for x := left; x < right; x += 1 {
		for y := top; y < bottom; y += 1 {
			coords = append(coords, Coordinate{x, y})
		}
	}
	return
}

func Parse(path string) (rects []Rect) {
	f, err := os.Open(path)
	if err != nil {
		os.Exit(1)
	}
	defer f.Close()

	re := regexp.MustCompile(`#(\d+)\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)`)

	nums := make([]int, 5)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		r := Rect{}
		line := scanner.Text()
		if tokens := re.FindStringSubmatch(line); tokens != nil {
			for i := 1; i < 6; i += 1 {
				if num, err := strconv.Atoi(tokens[i]); err == nil {
					nums[i-1] = num
				} else {
					fmt.Println("Could not get an integer in " + line)
					os.Exit(2)
				}
			}
			r.Id = nums[0]
			r.left = nums[1]
			r.top = nums[2]
			r.width = nums[3]
			r.height = nums[4]
			rects = append(rects, r)
		} else {
			fmt.Println("Could not parse " + line)
			os.Exit(1)
		}
	}
	return
}
