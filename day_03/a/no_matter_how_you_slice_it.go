package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_03"
)

func main() {
	rects := day_03.Parse("../input.txt")

	overlaps := make(map[int]map[int]int)
	for x := 0; x < len(rects)-1; x += 1 {
		for y := x + 1; y < len(rects); y += 1 {
			first_rect := rects[x]
			second_rect := rects[y]
			overlap := first_rect.Overlap(second_rect)
			for _, coord := range overlap {
				if overlaps[coord.X] == nil {
					overlaps[coord.X] = make(map[int]int)
				}
				overlaps[coord.X][coord.Y] = 1
			}
		}
	}

	total_overlap := 0
	for _, rows := range overlaps {
		for _, _ = range rows {
			total_overlap += 1
		}
	}

	fmt.Println(total_overlap)
}
